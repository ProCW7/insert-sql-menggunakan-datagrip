create table departemen
(
    id   int auto_increment
        primary key,
    nama varchar(100) not null
);

create table karyawan
(
    id           int auto_increment
        primary key,
    nama         varchar(50) not null,
    jenisKelamin varchar(1)  not null,
    status       varchar(8)  not null,
    tanggalLahir date        not null,
    tanggalMasuk date        not null,
    Departemen   int         not null
);


